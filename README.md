# aproviz
An Android Configuration Tool

## features
- Configures...
    - Element
    - Mumla
    - PhoneTrack
    - OsmAnd
    - Fennec
- Enables security features
    - Encryption
    - Disable Bluetooth
    - Hide Notifications
    - Restrict USB
- Template based configuration files for devices

## maintainer
Matrix: @leevi:systemli.org

## setup
```bash
# install system dependencies
sudo apt install android-tools-adb python3 python3-virtualenv curl wget unzip git apksigner zipalign tor
```

## usage

1. create templates for groups of similar phones. you can use `extends: parent.yml` to extend an existing template.
2. for each device run `./aproviz-cli` and follow the instructions.

```
usage: ./aproviz-cli [-h] [--template TEMPLATE] [--serial SERIAL] [--rerun] [--print] [--keep-adb] [--offline] [--cache-only]
                     [MODULE [MODULE ...]]

positional arguments:
  MODULE               specify the modules. defaults to all. Available modules: mumla, element, extra_apps, phonetrack,
                       osmand, contacts, system, sip, launcher

optional arguments:
  -h, --help           show this help message and exit
  --template TEMPLATE  The template to use
  --serial SERIAL      The device to provision
  --rerun              Run again, even if there is no change
  --print              Only print out template
  --keep-adb           Don't disable ADB
  --offline            Skip downloads and use cached files
  --cache-only         Only download files for later use in offline mode

```




