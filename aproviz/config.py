import os.path
import sys
from pathlib import Path

import yaml

config_file = os.path.join(os.path.dirname(__file__), '../config.yml')

try:
    CONFIG = yaml.load(Path(config_file).read_text(), Loader=yaml.FullLoader)
except yaml.YAMLError as exc:
    print("Error in config.yml:")
    print(exc)
    sys.exit(1)
