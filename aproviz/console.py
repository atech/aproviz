import importlib
import sys
import traceback

from aproviz.devices import get_devices
from aproviz.templates import get_template_list
from aproviz.utils.console import print_key_value, print_error
from colorama import init, Fore, Style
from time import sleep
init()
import argparse

def parse_args(available_modules):
    parser = argparse.ArgumentParser()
    parser.add_argument('--template', help='The template to use')
    parser.add_argument('--serial', help='The device to provision')
    parser.add_argument('--rerun', help='Run again, even if there is no change', action='store_true')
    parser.add_argument('--print', help='Only print out template', action='store_true')
    parser.add_argument('--keep-adb', help="Don't disable ADB", action='store_true')
    parser.add_argument('--offline', help="Skip downloads and use cached files", action='store_true')
    parser.add_argument('--cache-only', help="Only download files for later use in offline mode", action='store_true')
    choices = list(available_modules)
    choices.append([])
    parser.add_argument('modules', metavar='MODULE', nargs='*', help='specify the modules. defaults to all. Available '
                                                                     'modules: ' + ', '.join(available_modules),
                        choices=choices)
    return parser.parse_args()

def is_int(value):
  try:
    int(value)
    return True
  except:
    return False


def print_devices(devices, modules):
    print(Style.BRIGHT+"Devices: "+Style.RESET_ALL)
    i = 1
    for d in devices:
        serial = d['serial']
        path = d['path']
        model =  d['model'] + " ("+d['product']+")"


        if d['is_locked']:
            primary = "\033[9m"+serial + Style.RESET_ALL + " [" +path+ "]"
        else:
            primary = Fore.GREEN+serial+ Style.RESET_ALL + " [" +path+ "]"


        succeeded_modules_count = len(d['succeeded_modules'].keys())
        if succeeded_modules_count:
            progress = ('●' * succeeded_modules_count) + ('○' * (len(modules)-succeeded_modules_count))
        else:
            progress = ""

        if d['is_locked']:
            progress += " [inuse]"
        print(
            str(i).rjust(2)+': '+
            primary.ljust(36)+
            model.ljust(20)+
            Fore.CYAN+d['animal'].ljust(20)+d['color'].ljust(20)+
            Style.RESET_ALL+
            progress
        )
        i += 1
    print("")

def device_prompt(modules):
    devices = get_devices()
    if not len(devices):
        print("could not find any devices. make sure the device is connected and ADB is enabled")
        while not len(devices):
            sleep(1)
            devices = get_devices()

    print_devices(devices, modules)


    index = input(Fore.MAGENTA+"> please pick a device: "+Style.RESET_ALL)

    while not is_int(index) or int(index) < 1 or int(index) > len(devices):
        print("invalid number provided.")
        index = input(Fore.MAGENTA+"> please pick a device: "+Style.RESET_ALL)

    return devices[int(index)-1]


def template_prompt():
    templates = get_template_list()

    i = 1
    print()
    print(Style.BRIGHT+"Templates: "+Style.RESET_ALL)

    for t in templates:
        print(
            str(i).rjust(2)+': '+
            Fore.GREEN+t+
            Style.RESET_ALL
        )
        i += 1

    index = input(Fore.MAGENTA+"> please pick a template: "+Style.RESET_ALL)

    while not is_int(index) or int(index) < 1 or int(index) > len(templates):
        print("invalid number provided.")
        index = input(Fore.MAGENTA+"> please pick a template: "+Style.RESET_ALL)

    return templates[int(index)-1]

def print_device_info(device, templates):
    d = device['d']

    print('------------------------------')
    serial = d.shell(['getprop', 'ro.serialno'])
    print_key_value('Serial', serial)
    if serial != d.serial:
        print_key_value('Connection', d.serial)
    print_key_value('IMEI', d.shell("service call iphonesubinfo 1 | cut -c 52-66 | tr -d '.[:space:]'"))
    print_key_value('Model',d.shell(['getprop', 'ro.product.model']))
    print_key_value('Name',d.shell(['getprop', 'ro.product.name']))
    print_key_value('SoC', d.shell(['getprop', 'ro.hardware']))
    print_key_value('Flavour', d.shell(['getprop', 'ro.build.flavor']))
    print_key_value('Version', d.shell(['getprop', 'ro.modversion']))
    print_key_value('Android', d.shell(['getprop', 'ro.build.version.release']))
    print_key_value('SecPatch', d.shell(['getprop', 'ro.build.version.security_patch']))
    print_key_value('VendorSec', d.shell(['getprop', 'ro.lineage.build.vendor_security_patch']))
    print('------------------------------')
    print_key_value('Animal', device['animal'])
    print_key_value('Color', device['color'])
    t = list(templates)
    t.reverse()
    print_key_value('Templates', (Fore.BLUE+' <- '+Style.RESET_ALL).join(t))
    print('------------------------------')


def retry_module(module: str, e: Exception):
    """
    Reload failed module if the user likes to. Exit otherwise

    Parameters
    ----------
    module: str
        name of the failed module
    e: Exception
        Exception thrown by module
    """
    traceback.print_exception(e)
    print_error(str(e))
    print(
        "we can try again running this module.\nNote, that you can make changes to the module before executing")
    choice = input(Fore.MAGENTA + '> try again? (Y/n): ' + Style.RESET_ALL)
    if choice != "y" and choice != "Y" and choice != "":
        sys.exit(1)

    importlib.reload(importlib.import_module('aproviz.modules.' + module))
