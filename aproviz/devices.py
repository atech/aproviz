from adbutils import adb
import re
import adbutils.errors
from aproviz.utils.console import print_title, print_step
from aproviz.utils.namegenerator import str_to_animal, str_to_color
from aproviz.utils.preparation import screen_timeout, disable_rotation, unlock
from aproviz.utils.finishing import remove_uiautomator
from aproviz.utils.apk import apk_install
from colorama import init, Fore, Style
from pathlib import Path
import os.path
from time import sleep
import sys 

DEVICE_STATE_DIR = os.path.join(os.path.dirname(__file__), '../cache/device_state')

AndroidSdk = {
    'Lollipop': 21, # 5
    'Marshmallow': 23, # 6
    'Nougat': 24, # 7
    'Oreo': 26, # 8
    'Pie': 28, # 9
    'Ten': 29, # 10,
    'Eleven': 30, # 11
    'Lineage17_1': 29, # 10
    'Lineage18_1': 30, # 11,
    'Lineage19_1': 32 # 12
}


def get_prop(d, key):
    return d.shell(['getprop', key]).output.strip()

def get_devices(find_serial=None):
    devices = []
    for d in adb.device_list():
        print('checking '+d.serial)
        
        
        try:
            serial = d.shell('getprop ro.serialno', timeout=3).strip()
        except adbutils.errors.AdbTimeout:
            print("device seems offline. skipping")
            continue
        if find_serial and serial != find_serial:
            continue

        model = d.shell('getprop ro.product.model')
        product = d.shell('getprop ro.build.product')
        is_over_wifi = serial != d.serial
        is_locked = d.shell('cat /data/local/tmp/aproviz.lock').find("No such file or directory") == -1
        path = 'wifi' if is_over_wifi else d.get_devpath()

        succeeded_modules = dict()
        try:
            lines = Path(os.path.join(DEVICE_STATE_DIR, serial)).read_text().strip().split('\n')
            for line in lines:
                [module, config] = line.split(':', 1)
                succeeded_modules[module] = config
            
        except FileNotFoundError:
            pass

        devices.append({
            'd': d,
            'serial': serial,
            'path': path,
            'model': model,
            'product': product,
            'animal': str_to_animal(serial),
            'color': str_to_color(serial),
            'is_locked': is_locked,
            'succeeded_modules': succeeded_modules
        })

    return devices


def lock_device(d):
    d.shell('touch /data/local/tmp/aproviz.lock')

def store_module_success(serial, module, config_json):
    Path(DEVICE_STATE_DIR).mkdir(exist_ok=True, parents=True)
    file = os.path.join(DEVICE_STATE_DIR, serial)
    with open(file, 'a') as file1:
        file1.write(module+":"+config_json+"\n")


def prepare_device(d, old_password=""):
    print_title("prepare device")
    d.adb_device.adb_output('wait-for-device')

    print_step("start uiautomator")
    try:
        d.start_uiautomator()
    except Exception as e:
        print("error while starting uiautomator. sometimes this is due to a missing wifi connection")
        raise e
    
    d.unlock()

    if old_password:
        print_step("remove password")
        d.shell(['locksettings', 'clear', '--old', old_password])

        # shutoff screen again -> reload lock screen
        sleep(.5)
        d.shell(['input', 'keyevent', '26'])
        sleep(.5)
        d.shell(['input', 'keyevent', '26'])

    print_step("increase screen timeout")
    screen_timeout(d, 5*60)

    print_step("disable rotation")
    disable_rotation(d)

    print_step("activate airplane mode")
    d.shell(['settings', 'put', 'global', 'airplane_mode_on', '1'])
    d.shell(['svc', 'wifi', 'enable'])


    print_step("unlock screen")
    unlock(d)

def get_current_wifi(d):
    connectivity = d.shell(['dumpsys', 'connectivity']).output.strip()
    m = re.findall(r"SSID: \"(.*?)\"", connectivity)
    if len(m):
        return m[0]
    else:
        return None


def connect_wifi(d, ssid, password):
    print_step("connect to wifi")
    
    cur = get_current_wifi(d)
    if cur == ssid:
        print("already connected to the correct wifi. skipping")
        return

    apk_install(d, "assets/adb-join-wifi.apk")

    d.shell('am start -n com.steinwurf.adbjoinwifi/.MainActivity \
    -e ssid "%s " -e password_type WPA -e password "%s"' % (ssid, password))

    while True:
        sleep(1)
        cur = get_current_wifi(d)
        if cur:
            break

    sleep(2)

def get_locale(d):
    locale = get_prop(d, "persist.sys.locale")
    if locale == '':
        return get_prop(d, "ro.product.locale")
    else:
        return locale

def do_checks(d):
    if get_prop(d, 'ro.crypto.state') != "encrypted":
        raise Exception("device is unencrypted, but aproviz expects encryption to be turned on")

    res = d.shell("ping -c 1 google.com")
    if res.exit_code != 0:
        raise Exception("No internet connection. Ensure internet is working and try again")

    while get_locale(d) != 'en-US':
        d.shell('am start -a android.settings.LOCALE_SETTINGS')

        print("aproviz will only work with locale 'English (United States)'. please set the locale manually")
        input(Fore.MAGENTA + '> press enter to continue ' + Style.RESET_ALL)


def finish_device(d):
    print_title("finish device")
    adb_d = d.adb_device

    print_step("decrease screen timeout")
    screen_timeout(d, 1*60)

    print_step("remove uiautomator")
    remove_uiautomator(d)

    print_step("remove lock file")
    adb_d.shell('rm /data/local/tmp/aproviz.lock')

    # TODO: disable rooted debugging

