from aproviz.devices import get_prop
import datetime
import re
import os.path
from pathlib import Path

log_file = os.path.join(os.path.dirname(__file__), '../log.csv')

def log_device(device, templates):
    d = device['ui_d']

    # android = get_prop(d, 'ro.build.version.release')
    imeis = [
        d.shell("service call iphonesubinfo 1 | cut -c 52-66 | tr -d '.[:space:]'").output.strip(),
        d.shell("service call iphonesubinfo 2 | cut -c 52-66 | tr -d '.[:space:]'").output.strip()
    ]

    # Android >= 12 doesn't support `service call iphonesubinfo` anymore
    if len(imeis[0]) != 15:
        d.shell(['am', 'start', '-n', 'com.android.settings/.Settings$MyDeviceInfoActivity'])
        d(scrollable=True).scroll.to(textMatches="Android version", steps=3)
        xml = d.dump_hierarchy()
        imeis = re.findall("<node index=\"1\" text=\"(\d{15})\"", xml)
        d.press('back')
        d.press('back')
        
    data = [
        datetime.datetime.now().isoformat(),
        get_prop(d, 'ro.serialno'),
        imeis[0] if len(imeis) > 0 else "",
        imeis[1] if len(imeis) > 1 else "",
        get_prop(d, 'ro.product.brand'),
        get_prop(d, 'ro.product.model'),
        get_prop(d, 'ro.modversion'),
        ','.join(templates),
        device['animal'],
        device['color']
    ]
    data2 = []
    for el in data:
        data2.append('"'+str(el)+'"')
    line = ";".join(data2)

    with open(log_file, "a+")  as file:
        file.write(line+"\n")