from os import walk, path
from aproviz.devices import get_prop
from aproviz.utils.console import print_warning
from importlib import import_module


def get_module_list():
    _, _, files = next(walk('./aproviz/modules'))
    available_modules = []
    for file in files:
        if file == '__init__.py':
            continue
        available_modules.append(path.splitext(file)[0])
    return available_modules
    

def execute_module(d, module, config):
    # execute module
    m = import_module('aproviz.modules.'+module)
    
    m.provision(d, config)
