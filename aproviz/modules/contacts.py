from time import sleep
from aproviz.devices import AndroidSdk
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.console import print_step, print_success
from aproviz.utils.permissions import ignore_battery_optimizations, grant_permission
from pprint import pprint

tested = [
    AndroidSdk['Lineage17_1'],
    AndroidSdk['Lineage18_1'],
    AndroidSdk['Lineage19_1']
]


def provision(d, config):
    d.shell(['pm', 'clear', 'com.android.providers.contacts'])

    changes = False
    if 'remote' in config and config['remote']:
        fdroid_install(d, 'at.bitfire.davdroid')

        
        grant_permission(d, "at.bitfire.davdroid", 'android.permission.READ_CONTACTS')
        grant_permission(d, "at.bitfire.davdroid", 'android.permission.WRITE_CONTACTS')
        ignore_battery_optimizations(d, 'at.bitfire.davdroid')

        print_step("add remote dav")
        d.app_stop('at.bitfire.davdroid')
        d.shell([
            'am', 'start',
            '-n', 'at.bitfire.davdroid/at.bitfire.davdroid.ui.setup.LoginActivity',
            '-e', 'url', config['remote']['url'],
            '-e', 'username', config['remote']['username'],
            '-e', 'password', config['remote']['password']
        ])
        d(resourceId="at.bitfire.davdroid:id/login").click()
        d(resourceId="at.bitfire.davdroid:id/accountName").wait(timeout=20)
        d(resourceId="at.bitfire.davdroid:id/accountName").set_text(config['remote']['name'])
        d(resourceId="at.bitfire.davdroid:id/create_account").click()

        d(resourceId="at.bitfire.davdroid:id/action_overflow").wait()
        print_step("activate all contact lists")
        i = 1
        while True:
            box = d.xpath('//*[@resource-id="at.bitfire.davdroid:id/list"]/android.widget.LinearLayout['+str(i)+']/android.widget.CheckBox')
            if not box.exists:
                break
            box.click()
            sleep(1)
            i += 1
        
        print_step("start syncing")
        d(resourceId="at.bitfire.davdroid:id/sync", className="android.widget.ImageButton").click()

        print_success("contacts will be synced from now on")


    if 'local' in config and config['local'] and len(config['local']):
        for contact in config['local']:
            print_step('add contact '+contact['name'])
            d.shell([
                'am', 'start',
                '-a', 'android.intent.action.INSERT',
                '-t', 'vnd.android.cursor.dir/contact',
                # '-e', 'name', '"'+name+'"',
                # '-e', 'phone', '"'+number+'"'
                '-e', 'name', contact['name'],
                '-e', 'phone', contact['number']
            ])
            sleep(.5)
            d(resourceId="com.android.contacts:id/kind_icon").exists(timeout=1)

            # on crDroid / Android 13 there is prompt for adding a google account
            if d(resourceId="com.android.contacts:id/text", textMatches="Take a minute.*").exists:
                d(resourceId="com.android.contacts:id/left_button").click() # cancel

            # set default adress bock
            if d(textMatches="Device", resourceId="android:id/text1").exists:
                d(textMatches="Device", resourceId="android:id/text1").click()

        
            sleep(.5)
            d(resourceId="com.android.contacts:id/editor_menu_save_button").click()

        #     if d(resourceId="com.android.contacts:id/editor_menu_save_button").exists:
        #         d(resourceId="com.android.contacts:id/editor_menu_save_button").click()
        #     else:
        #         if android < 8:
        #             d(resourceId="com.android.contacts:id/menu_save").click()
        #         else:
        #             d(resourceId="com.android.contacts:id/save_menu_item").click()

            d(resourceId="com.android.contacts:id/multiscroller").wait()
            d.press('back')
            sleep(.5)
            changes = True

        print_success("contacts successfully added")

