import json
import re
from time import sleep
from urllib.parse import quote

from colorama import Fore, Style

from aproviz import state
from aproviz.oidc import oidc_login
from aproviz.utils.console import print_step, print_substep, print_warning, print_debug
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.permissions import grant_permission, ignore_battery_optimizations

def get_next_opened_url(d):
    while True:
        starter = d.shell(['dumpsys', 'activity', 'starter']).output.strip()
        m = re.findall(r"act=android\.intent\.action\.VIEW dat=(http.*?) ", starter)
        if len(m):
            return m[0]
        sleep(1)

def cache(config):
    if not 'enabled' in config or not config['enabled']:
        print("element is not enabled. skipping")
        return

    print_step("Caching Element apk...")
    fdroid_install(None, 'im.vector.app', cache_only=True)


def provision(d, config):
    if not 'enabled' in config or not config['enabled']:
        print("element is not enabled. skipping")
        return

    fdroid_install(d, 'im.vector.app')

    print_step("grant permissions")
    grant_permission(d, "im.vector.app", 'android.permission.CAMERA')
    grant_permission(d, "im.vector.app", 'android.permission.READ_EXTERNAL_STORAGE')
    grant_permission(d, "im.vector.app", 'android.permission.RECORD_AUDIO')
    grant_permission(d, "im.vector.app", 'android.permission.POST_NOTIFICATIONS')
    ignore_battery_optimizations(d, 'im.vector.app')

    d.app_stop('im.vector.app')
    d.app_start('im.vector.app', use_monkey = True)
    d.app_wait('im.vector.app')

    if not is_loggedin(d, config):
        d.app_clear('im.vector.app')
        grant_permission(d, "im.vector.app", 'android.permission.CAMERA')
        grant_permission(d, "im.vector.app", 'android.permission.READ_EXTERNAL_STORAGE')
        grant_permission(d, "im.vector.app", 'android.permission.RECORD_AUDIO')
        grant_permission(d, "im.vector.app", 'android.permission.POST_NOTIFICATIONS')
        d.app_start('im.vector.app', use_monkey = True)
        d.app_wait('im.vector.app')

        if is_account_existing(d, config['server'], config['username']):
            login(d, config)

            # we assume that direct contacts are already created
            # to avoid duplicated rooms
            if 'direct_contact' in config and 'always_add_contacts' in config and config['always_add_contacts']:
                add_contacts(d, config['direct_contact'])
        else:
            register(d, config)

            if 'direct_contact' in config:
                add_contacts(d, config['direct_contact'])


def is_loggedin(d, config):
    if d(resourceId="im.vector.app:id/action_bar_root").exists(timeout=3):
        if d(resourceId="im.vector.app:id/loginFragmentContainer").exists(timeout=1):
            return False
        
        d(resourceId="im.vector.app:id/avatar").wait()
        sleep(.5)
        d(resourceId="im.vector.app:id/avatar").click()
        sleep(.5)

        if d(text="Skip").exists:
            d(text="Skip").click()
            sleep(.5)
            d(resourceId="im.vector.app:id/avatar").click()

        d(text="General").click()
        d(resourceId="im.vector.app:id/recycler_view").scroll.to(text="Homeserver")
        loggedin_id = d(textMatches="@.*:.*").info['text']
        expected = "@%s:%s" % (config['username'], config['server'])
        print(expected, loggedin_id)
        d.press("back")
        d.press("back")
        if loggedin_id != expected:
            print("%s is loggedin, but we expect %s -> logging out" % (loggedin_id, expected))
            return False
        else:
            return True
    else:
        return False

def is_account_existing(d, server, username):
    if state.offline:
        print_warning(f"Can not check if the account {username} exists on {server} in offline mode")
        result = input(Fore.MAGENTA + f"> Is {username} already registered on {server}? [y/n] " + Style.RESET_ALL).lower().strip()
        return result == "y"

    try:
        well_known = d.shell(['curl', '-s', '-k', '-L', "https://%s/.well-known/matrix/client" % server]).output
        well_known_dict = json.loads(well_known)
        host = well_known_dict['m.homeserver']['base_url']
    except json.decoder.JSONDecodeError:
        host = 'https://' + server
    res = d.shell(['curl', '-s', '-k', host+'/_matrix/client/r0/register/available?username='+quote(username)]).output

    try:
        res_dict = json.loads(res)
        print(res_dict)
        if 'error' in res_dict and res_dict['error'] == 'Registration has been disabled':
            print_debug("Registration is disabled on this homeserver. we just assume that the accounts must then exist.")
            return True
        
        if 'errcode' in res_dict and res_dict['errcode'] != 'M_USER_IN_USE':
            raise Exception("could not check whether account is available @ "+server+'. server responded with: '+res)

        return 'available' not in res_dict or not res_dict['available']
    except json.decoder.JSONDecodeError:
        print(res)
        raise Exception("could not check whether account is available @ "+server)

def register(d, config):
    print_step("register account")
    # start
    d(resourceId="im.vector.app:id/loginSplashSubmit").click()
    d(resourceId="im.vector.app:id/useCaseOptionOne").click()

    # choose server
    _change_server(d, config['server'])

    d(resourceId="im.vector.app:id/createAccountEditText").set_text(config['username'])
    d(resourceId="im.vector.app:id/createAccountPassword").set_text(config['password'])
    d.press("enter")
    d.press("enter")

   # wait for registration to go to next step
    while True:
        if d(resourceId="im.vector.app:id/accountCreatedTakeMeHome").exists:
            break
        if d(resourceId="im.vector.app:id/emailEntrySubmit").exists:
            break
        if d(resourceId="im.vector.app:id/adapter_item_policy_checkbox").exists:
            break
        if d(resourceId="im.vector.app:id/captchaRoot").exists:
            break
        sleep(0.5)

    # email
    if d(resourceId="im.vector.app:id/emailEntrySubmit").exists:
        print_substep("email verification")
        if 'email' not in config:
            raise Exception('server requires email verification, but no email provided. Add a email address and try again')
        d(className="android.widget.EditText").set_text(config['email'])
        d(resourceId="im.vector.app:id/emailEntrySubmit").click()
        print("> please follow the instructions in the mail to "+config['email'])
        sleep(2)
        d(resourceId="im.vector.app:id/emailVerificationResendEmail").wait()
        d(resourceId="im.vector.app:id/emailVerificationResendEmail").wait_gone(timeout=5*60)
        if d(resourceId="im.vector.app:id/emailVerificationResendEmail").exists:
            raise Exception("email verification timeout. please try again")
        print("email address verified")

        # wait for registration to go to next step
        while True:
            if d(resourceId="im.vector.app:id/accountCreatedTakeMeHome").exists:
                break
            if d(resourceId="im.vector.app:id/adapter_item_policy_checkbox").exists:
                break
            if d(resourceId="im.vector.app:id/captchaRoot").exists:
                break
            sleep(0.5)

    if d(resourceId="im.vector.app:id/adapter_item_policy_checkbox").exists:
        print_substep("accept policies")
        d(resourceId="im.vector.app:id/adapter_item_policy_checkbox").click()
        d(resourceId="im.vector.app:id/termsSubmit").click()

        # wait for registration to go to next step
        while True:
            if d(resourceId="im.vector.app:id/accountCreatedTakeMeHome").exists:
                break
            if d(resourceId="im.vector.app:id/captchaRoot").exists:
                break
            sleep(0.5)


    if d(resourceId="im.vector.app:id/captchaRoot").exists:
        print_substep("captcha")
        print("> please solve the captcha")
        d(resourceId="im.vector.app:id/captchaRoot").wait_gone(timeout=120)
        d(resourceId="im.vector.app:id/accountCreatedTakeMeHome").wait(timeout=20)

    d(resourceId="im.vector.app:id/accountCreatedTakeMeHome").click()
    d(resourceId="im.vector.app:id/later").click()

def _change_server(d, server):
    if server == 'matrix.org':
        # this is already the default
        return
    d(resourceId="im.vector.app:id/editServerButton").click()
    d(className="android.widget.EditText").set_text(server)
    d.press("enter")
    d.press("enter")

def login(d, config):
    print_step("login into existing account")

    d(resourceId="im.vector.app:id/loginSplashAlreadyHaveAccount").click()
    _change_server(d, config['server'])

    oidc = False

    d(resourceId="im.vector.app:id/loginRoot").wait()
    if d(resourceId="im.vector.app:id/loginEditText").exists:
        # login with username & password directly
        d(resourceId="im.vector.app:id/loginEditText").set_text(config['username'])
        d(resourceId="im.vector.app:id/loginPasswordEditText").set_text(config['password'])
        d(resourceId="im.vector.app:id/loginSubmit").click()

    elif d(resourceId="im.vector.app:id/ssoButtons").exists:
        d(resourceId="im.vector.app:id/ssoButtons").click()
        loginUrl = get_next_opened_url(d)

        # login
        authedUrl = oidc_login(loginUrl, config['username'], config['password'])

        oidc = True
        # pass URL to element
        d.shell([
            'am', 'start',
            '-a', 'android.intent.action.VIEW',
            '-d', authedUrl,
        ])

    else:
        raise Exception("could not find any method for login")



    d(resourceId="im.vector.app:id/later").click()

    if d(resourceId="im.vector.app:id/later").exists(timeout=2):
        d(resourceId="im.vector.app:id/later").click()

    # skip verification prompt
    print_substep("skip verification prompt")
    d(resourceId="im.vector.app:id/alerter_texts").wait(timeout=3)
    if d(resourceId="im.vector.app:id/alerter_texts").exists:
        d(resourceId="im.vector.app:id/alerter_texts").click()
        d(resourceId="im.vector.app:id/itemVerificationActionTitle", text="Skip").click()

    reset_keys(d, config, oidc)

def reset_keys(d, config, oidc):
    # reset keys
    print_substep("reset keys")
    d(resourceId="im.vector.app:id/avatar").click()
    d(resourceId="android:id/title", textMatches="(Sicherheit|Security).*").click()
    d(textMatches="(Cross-Signing)").click()
    d(resourceId="im.vector.app:id/itemGenericItemButton").click()
    if oidc:
        d(resourceId="im.vector.app:id/reAuthConfirmButton").click()
        # login again
        loginUrl = get_next_opened_url(d)
        # login
        oidc_login(loginUrl, config['username'], config['password'])
        d.press("back")

    d(resourceId="im.vector.app:id/waiting_view_content").wait_gone()
    d.press("back")

    # remove existing sessions
    print_substep("remove existing sessions")
    d(resourceId="im.vector.app:id/recycler_view").scroll.to(textMatches="(Show All Sessions)")
    d(textMatches="(Show All Sessions)").click()
    d(resourceId="im.vector.app:id/itemDeviceTrustLevelIcon").wait()
    while d(description="Warning trust level").exists:
        d(description="Warning trust level").click()
        d(textMatches="Sign out.*").click()
        if oidc:
            d(resourceId="im.vector.app:id/reAuthConfirmButton").click()
            # login again
            loginUrl = get_next_opened_url(d)
            # login
            oidc_login(loginUrl, config['username'], config['password'])

            d.press("back")
            
        d(resourceId="im.vector.app:id/waiting_view_content").wait_gone()
        sleep(1)
    d(description="Navigate up").click()
    d(description="Navigate up").click()
    d(description="Navigate up").click()


def add_contacts(d, contacts):
    print_step("start direct chats")
    d.app_start('im.vector.app', use_monkey = True)
    d.app_wait('im.vector.app')
    sleep(3)
    for matrixid in contacts:
        print_substep("start direct chat with " + matrixid)
        d(resourceId="im.vector.app:id/newLayoutCreateChatButton").click()
        sleep(0.5)
        d(resourceId="im.vector.app:id/start_chat").click()
        d(resourceId="im.vector.app:id/userListSearch").set_text(matrixid)
        sleep(1)
        d(resourceId="im.vector.app:id/knownUserName").click()
        d(resourceId="im.vector.app:id/action_create_direct_room").click()
        d(resourceId="im.vector.app:id/roomToolbar").wait()
        sleep(1)
        if d(text="START CHAT ANYWAY").exists(timeout=1):
            d(text="START CHAT ANYWAY").click()
        # Send message
        d(resourceId="im.vector.app:id/composerEditText").set_text(f"Hey, this is an automatic request")
        d(resourceId="im.vector.app:id/sendButton").click()
        # Waiting for room
        overlay = d(resourceId="im.vector.app:id/waiting_view_content")
        if overlay.exists:
            overlay.wait_gone()
        d(resourceId="im.vector.app:id/roomToolbarTitleView").wait()
        # Exit
        d(description="Navigate up").click()
