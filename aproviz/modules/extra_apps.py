import os.path
import re

from aproviz.utils.apk import apk_install, apk_download
from aproviz.utils.console import print_step, print_substep, print_warning
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.permissions import grant_permission


def get_cache_file(url):
    filename = re.sub('[^a-zA-Z0-9\-_\.]+', '_', url)
    return os.path.join(os.path.dirname(__file__), '../../cache', filename)


def cache(config):
    if 'fdroid' in config:
        print_step("Caching some fdroid apps...")

        for app in config['fdroid']:
            fdroid_install(None, app, cache_only=True)

    if 'aurora' in config:
        print_step("Caching some aurora apps...")

        if 'fdroid' not in config or 'com.aurora.store' not in config['fdroid']:
            fdroid_install(None, 'com.aurora.store', cache_only=True)

        if len(config['aurora']):
            print_warning("automatic installing of aurora apps doesn't work right now. you should do it manually")

    if 'urls' in config:
        print_step("Caching some apps from URLs...")

        for url in config['urls']:
            apk_file = get_cache_file(url)
            apk_download(apk_file, url)


def provision(d, config):
    if 'fdroid' in config:
        print_step("installing some fdroid apps...")

        for app in config['fdroid']:
            fdroid_install(d, app, debuggable=False)

    if 'aurora' in config:
        print_step("installing some aurora apps...")

        if 'fdroid' not in config or 'com.aurora.store' not in config['fdroid']:
            fdroid_install(d, 'com.aurora.store')

        print_substep('grant permissions')
        d.app_clear('com.aurora.store')
        grant_permission(d, "com.aurora.store", 'android.permission.READ_EXTERNAL_STORAGE')
        grant_permission(d, "com.aurora.store", 'android.permission.WRITE_EXTERNAL_STORAGE')

        print_substep('aurora setup')
        d.app_start('com.aurora.store')
        d(resourceId="com.aurora.store:id/checkbox_accept").click()
        d(resourceId="com.aurora.store:id/btn_primary").click()
        d(resourceId="com.aurora.store:id/btn_forward").click()
        d(resourceId="com.aurora.store:id/btn_forward").click()
        d(resourceId="com.aurora.store:id/btn_forward").click()
        d(resourceId="com.aurora.store:id/btn_forward").click()
        d(resourceId="com.aurora.store:id/btn_forward").click()
        d(resourceId="com.aurora.store:id/btn_anonymous").click()
        d(resourceId="com.aurora.store:id/search_fab").wait()

        if len(config['aurora']):
            print_warning("automatic installing of aurora apps doesn't work right now. you should do it manually")

        for app in config['aurora']:
            # d.shell([
            #     'am', 'start',
            #     '-a', 'android.intent.action.VIEW',
            #     '-d', 'https://play.google.com/store/apps/details?id='+app,
            #     'com.aurora.store'
            # ])

            pass

    if 'urls' in config:
        print_step("installing some apps from URLs...")

        for url in config['urls']:
            apk_file = get_cache_file(url)
            apk_download(apk_file, url)

            print("installing " + url)
            apk_install(d, apk_file)
