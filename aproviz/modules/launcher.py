from aproviz.devices import AndroidSdk
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.console import print_step, print_error
from aproviz.utils.sqlite3 import sqlite3_query
from aproviz.utils import shared_prefs
from uiautomator2.exceptions import UiObjectNotFoundError
import sys
from time import sleep

def provision(d,config):
    if not 'icons' in config or not config['icons']:
        print("launcher is not enabled. skipping")
        return

    fdroid_install(d, "org.fossify.home", debuggable=True)

    # set launcher
    d.shell(['pm', 'set-home-activity', 'org.fossify.home/.activities.MainActivity'])

    d.shell(['am', 'force-stop', 'org.fossify.home'])
    d.shell(['input', 'keyevent', 'KEYCODE_HOME'])
    d.app_start('org.fossify.home')
    d.app_wait('org.fossify.home')
    sleep(2)

    prefs = shared_prefs.SharedPrefs(d, 'org.fossify.home', 'Prefs.xml')
    prefs.Load()
    prefs.SetInt("home_column_count", 5)
    prefs.SetInt("home_row_count", 5)
    prefs.Commit()


    # clear launcher
    sqlite3_query(d,
        "org.fossify.home",
        "apps.db",
        "DELETE FROM home_screen_grid_items"
    )

    for slot, app in config['icons'].items():
        if not app:
            continue
        print("add "+app['title']+" @ "+slot)
        s = slot.split(':')

        if s[0] == 'workspace':
            container = -100
            cellX = int(s[1])-1
            cellY = int(s[2])-1
            docked = 0
        elif s[0] == 'hotseat':
            container = -101
            cellX = int(s[1])-1
            cellY = 5
            docked = 1
        else:
            raise Exception("Unknown slot type '"+s[0]+"' for '"+slot+"'. Only 'workspace' and 'hotseat' are allowed")


        if 'package' in app:
            package_name = app['package']

        # template backwards compatibility
        if not package_name and 'component' in app:
            package_name = app['component'].split('/')[0]

        sqlite3_query(
            d, 
            "org.fossify.home",
            "apps.db",
            """
                INSERT INTO home_screen_grid_items (left, top, right, bottom, page, package_name, activity_name, title, type, class_name, widget_id, shortcut_id, docked)
                    VALUES ($cellX, $cellY, $cellX, $cellY, 0, '$package_name', '', '$title', 0, '', -1, '', $docked)
            """, {
                'title': app['title'],
                'package_name': package_name,
                'docked': docked,
                'cellX': cellX,
                'cellY': cellY,
            }
        )

    # restart launcher
    d.shell(['am', 'force-stop', 'org.fossify.home'])
    d.shell(['input', 'keyevent', 'KEYCODE_HOME'])
