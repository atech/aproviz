from time import sleep
from aproviz.devices import AndroidSdk
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.console import print_step, print_substep
from aproviz.utils.permissions import grant_permission, ignore_battery_optimizations
from aproviz.utils.sqlite3 import sqlite3_query
from aproviz.utils import shared_prefs


tested = [
    AndroidSdk['Lineage17_1'],
    AndroidSdk['Lineage18_1'],
    AndroidSdk['Lineage19_1']
]

def cache(config):
    if 'enabled' not in config or not config['enabled']:
        print("mumla is not enabled. skipping")
        return

    print_step("Caching Mumla apk...")
    fdroid_install(None, 'se.lublin.mumla', cache_only=True)


def provision(d, config):
    if not 'enabled' in config or not config['enabled']:
        print("mumla is not enabled")
        return
    fdroid_install(d, 'se.lublin.mumla', debuggable=True)

    d.app_stop('se.lublin.mumla') 
    d.app_clear('se.lublin.mumla')

    print_step("grant permissions")
    grant_permission(d, "se.lublin.mumla", 'android.permission.RECORD_AUDIO')
    ignore_battery_optimizations(d, 'se.lublin.mumla')

    print_step("generate config files & certificate")
    d.app_start('se.lublin.mumla')
    d(resourceId="android:id/button1").click()
    d(resourceId="se.lublin.mumla:id/menu_add_server_item").wait()

    d.app_stop('se.lublin.mumla') 

    print_step("Add server")
    for i, server in enumerate(config['server']):
        id = i+1
        sqlite3_query(d, 'se.lublin.mumla', "mumble.db", """
            INSERT INTO server (_id, name, host, port, username, password)
                VALUES ($id, '$name', '$host', $port, '$username', '$password')
                ON CONFLICT(_id) DO UPDATE SET name='$name', host='$host', port=$port, username='$username', password='$password';
        """, {
            'id':id,
            'name':server['servername'],
            'host':server['host'],
            'port':server['port']  if 'port' in server else 64738,
            'username':server['username'],
            'password':server['password'] if 'password' in server else ''
        })
        if 'accesstokens' in server:
            for token in server['accesstokens']:
                sqlite3_query(d, 'se.lublin.mumla',
                    "mumble.db", 
                    "INSERT INTO tokens (value, server) VALUES ('$value', $server)",
                    {
                        'server': id,
                        'value': token
                    }
                )

    print_step("mumla settings")
    quality=(config['inputquality'] if 'inputquality' in config else 20000)
    packetlength=(config['packetlength'] if 'packetlength' in config else 60)
    inputmethod=(config['inputmethod'] if 'inputmethod' in config else 'voiceActivity')    

    prefs = shared_prefs.SharedPrefs(d, 'se.lublin.mumla', 'se.lublin.mumla_preferences.xml', )
    prefs.Load()
    prefs.SetString("audioInputMethod", inputmethod)
    prefs.SetString("input_quality", "44100")
    prefs.SetInt("input_bitrate", quality)
    prefs.SetString("audio_per_packet", round(packetlength/10))
    prefs.SetBoolean("load_images", False)
    prefs.Commit()


