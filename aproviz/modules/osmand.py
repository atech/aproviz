from time import sleep

from aproviz import state
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.console import print_step, print_substep
from aproviz.utils.permissions import grant_permission
from aproviz.utils.shared_prefs import SharedPrefs
from urllib.parse import urlencode
import os
import subprocess
import uiautomator2


def cache(config):
    if 'enabled' not in config or not config['enabled']:
        print("osmand is not enabled. skipping")
        return

    print_step("Caching osmand apks...")
    fdroid_install(None, 'net.osmand.plus', cache_only=True)
    fdroid_install(None, 'net.osmand.srtmPlugin.paid', cache_only=True)

    # add offline maps
    print_step('Caching maps...')
    for map in config['maps']:
        print_substep("add map " + map)
        download_map(map)


def download_map(map):
    map_file = os.path.join(os.path.dirname(__file__), '../../cache', map + '_2.obf')
    if not os.path.isfile(map_file):
        if state.offline:
            raise Exception(f"Offline mode failed: Map file not cached {map_file}")

        print("downloading " + map)
        subprocess.run(['wget', '-O', '/tmp/' + map + '.obf.zip',
                        'https://download.osmand.net/download?standard=yes&file=' + map + '_2.obf.zip'])
        subprocess.run(
            ['unzip', '-x', '/tmp/' + map + '.obf.zip', '-d', os.path.join(os.path.dirname(__file__), '../../cache')])
    return map_file


def provision(d, config):
    if not 'enabled' in config or not config['enabled']:
        print("osmand is not enabled. skipping")
        return
    
    fdroid_install(d, 'net.osmand.plus', debuggable=True)

    # deactivated because it is currently not supported by Android 14
    # Error: Failure [INSTALL_FAILED_DEPRECATED_SDK_VERSION: App package must target at least SDK version 23, but found 21]
    # fdroid_install(d, 'net.osmand.srtmPlugin.paid')

    print_step('start app and grant permissions')
    d.app_stop('net.osmand.plus') 
    d.app_clear('net.osmand.plus')
    grant_permission(d, "net.osmand.plus", 'android.permission.READ_EXTERNAL_STORAGE')
    grant_permission(d, "net.osmand.plus", 'android.permission.WRITE_EXTERNAL_STORAGE')
    grant_permission(d, "net.osmand.plus", 'android.permission.ACCESS_COARSE_LOCATION')
    grant_permission(d, "net.osmand.plus", 'android.permission.ACCESS_FINE_LOCATION')

    d.app_start('net.osmand.plus')
    d.app_wait('net.osmand.plus')
    try:
        d(resourceId="net.osmand.plus:id/action_bar_root").wait()
    except uiautomator2.exceptions.HTTPError:
        # print("got an uiautomator2.exceptions.HTTPError -> ignoring
        sleep(10)
    d.app_stop('net.osmand.plus')
    
    # add offline maps
    print_step('adding maps')
    for map in config['maps']:
        print_substep("add map "+map)
        map_file = download_map(map)

        # map already uploaded?
        output, exit_code = d.shell(['ls', '/sdcard/Android/data/net.osmand.plus/files/'+map+'.obf'])
        if exit_code != 0:
            print("uploading map "+map)
            d.adb_device.push(map_file, '/sdcard/Android/data/net.osmand.plus/files/'+map+'.obf')
    


    print_step('add online layer')
    prefs = SharedPrefs(d, 'net.osmand.plus', 'net.osmand.settings.xml')
    prefs.Load()
    prefs.SetString("enabled_plugins", "osmand.wikipedia,osmand.srtm.paid,osmand.openplacereviews,osmand.rastermaps")
    
    codename  = d.info['productName']
    if codename.startswith('jflte') or codename == "jfvelte" or codename == "jactivelte":
        # workaround for crashing OsmAnd with Galaxy S4
        # https://github.com/osmandapp/OsmAnd/issues/16878
        prefs.SetBoolean("use_opengl_render", False)
    
    prefs.Commit()


    for layer in config['online_layers']:
        obj = {
            'tile_size': 256,
            'img_density': 16,
            'avg_img_size': 32000,
            'ext': 'png'
        }
        merged = {**obj, **layer}

        content = ""
        for key, value in merged.items(): 
            content += "["+key+"]\n"+str(value)+"\n"

        d.adb_device.sync.push(
            content.encode(),
            "/sdcard/Android/data/net.osmand.plus/files/tiles/"+layer['name']+"/.metainfo"
        )

    # set overlay
    if 'overlay' in config:
        print_step('set overlay')
        prefs = SharedPrefs(d, 'net.osmand.plus', 'net.osmand.settings.default.xml')
        prefs.Load()
        prefs.SetString("map_overlay", config['overlay'])
        prefs.SetString("layer_transparency_seekbar_mode", "OVERLAY")
        prefs.SetInt("overlay_transparency", 110)
        prefs.Commit()
