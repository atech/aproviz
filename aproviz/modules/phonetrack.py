from time import sleep
from aproviz.utils.fdroid import fdroid_install
from aproviz.utils.console import print_step
from aproviz.utils.notifications import hide_notifications_on_lockscreen
from aproviz.utils.sqlite3 import sqlite3_query
from aproviz.utils.shared_prefs import SharedPrefs


def cache(config):
    if 'enabled' not in config or not config['enabled']:
        print("phonetrack is not enabled. skipping")
        return

    print_step("Caching phonetrack apks...")
    fdroid_install(None, 'net.eneiluj.nextcloud.phonetrack', cache_only=True)


def provision(d, config):
    if not 'enabled' in config or not config['enabled']:
        print("phonetrack is not enabled")
        return

    fdroid_install(d, 'net.eneiluj.nextcloud.phonetrack', debuggable=True)

    print_step('start app and grant permissions')
    d.app_stop('net.eneiluj.nextcloud.phonetrack')
    d.app_clear('net.eneiluj.nextcloud.phonetrack')
    d.shell(['pm', 'grant', "net.eneiluj.nextcloud.phonetrack", 'android.permission.ACCESS_FINE_LOCATION'])
    d.shell(['pm', 'grant', "net.eneiluj.nextcloud.phonetrack", 'android.permission.ACCESS_COARSE_LOCATION'])
    d.shell(['pm', 'grant', "net.eneiluj.nextcloud.phonetrack", 'android.permission.ACCESS_BACKGROUND_LOCATION'])

    # start app once to create config files & databases
    d.app_start('net.eneiluj.nextcloud.phonetrack')
    d(resourceId="net.eneiluj.nextcloud.phonetrack:id/home_toolbar").wait()
    sleep(.5)
    d.app_stop('net.eneiluj.nextcloud.phonetrack')


    print_step("add remotes")
    sqlite3_query(d,
        "net.eneiluj.nextcloud.phonetrack",
        "NEXTCLOUD_PHONETRACK",
        "DELETE FROM logjobs"
    )
    for remote in config['remotes']:
        sqlite3_query(d,
            "net.eneiluj.nextcloud.phonetrack",
            "NEXTCLOUD_PHONETRACK",
            """
                INSERT INTO logjobs (TITLE, URL, DEVICENAME, MINTIME, MINDISTANCE, MINACCURACY, KEEPGPSON, ENABLED, TOKEN)
                    VALUES ('$title', '$url', '$devicename', 3, 1, 50, 1, 1, '$token')
            """,
            {
                'title': remote['title'],
                'url': remote['url'],
                'devicename': remote['devicename'] if 'devicename' in remote else config['devicename'],
                'token': remote['token']
            }
        )


    print_step("enable autostart & disable offline tracking")
    prefs = SharedPrefs(d, 'net.eneiluj.nextcloud.phonetrack', 'net.eneiluj.nextcloud.phonetrack_preferences.xml')
    prefs.Load()
    prefs.SetBoolean("offlineModeAware", True)
    prefs.SetBoolean("autostart", True)
    prefs.Commit()

    print_step("hide notification")
    d.app_start('net.eneiluj.nextcloud.phonetrack')
    d(resourceId="net.eneiluj.nextcloud.phonetrack:id/home_toolbar").wait()
    sleep(1)
    hide_notifications_on_lockscreen(d, 'net.eneiluj.nextcloud.phonetrack', 'PhoneTrack')
