from aproviz.utils.console import print_step
from aproviz.devices import AndroidSdk, get_prop
import datetime
from time import sleep

def provision(d, config):
    enable_usb_restrict(d)
    hide_notification_content(d)
    disable_bluetooth_nfc_wifi(d)
    enable_gps(d)
    
    if 'password' in config:
        set_password(d, config['password'])

    if 'locale' in config:
        set_locale(d, config['locale'])

    if 'timezone' in config:
        set_timezone(d, config['timezone'])

    # set_time doesn't work without root
    # TODO: find new solution
    # set_time(d)



def hide_notification_content(d):
    print_step('hide notification content at lock screen')
    d.shell(['settings', 'put', 'secure', 'lock_screen_allow_private_notifications', '0'])

def disable_bluetooth_nfc_wifi(d):
    print_step("disable Bluetooth, NFC and Wifi")
    d.shell(['settings', 'put', 'global', 'bluetooth_on', '0'])
    d.shell(['settings', 'put', 'global', 'bluetooth_disabled_profiles', '1'])
    d.shell(['svc', 'bluetooth', 'disable'])
    d.shell(['svc', 'nfc', 'disable'])
    if d.serial.find(':') == -1:
        # disable only if we are not connected via wifi
        d.shell(['svc', 'wifi', 'disable'])

def enable_gps(d):
    print_step("enable GPS")
    d.shell(['settings', 'put', 'secure', 'location_mode', '3'])
    d.shell(['settings', 'put', 'secure', 'location_providers_allowed', '+gps'])

def set_password(d, password):
    print_step("set password")
    d.shell(['locksettings', 'set-password', password])
    pass

def enable_usb_restrict(d):
    print_step("Restrict USB")

    if get_prop(d, 'ro.build.user') == 'grapheneos':
        # graphene restricts USB by default
        return
    
    trust_restrict_usb = d.shell(['settings', 'get', '--lineage', 'global', 'trust_restrict_usb']).output.strip()
    if trust_restrict_usb == '1':
        return
    trust_restrict_usb = d.shell(['settings', 'get', '--lineage', 'secure', 'trust_restrict_usb']).output.strip()
    if trust_restrict_usb == '1':
        return

    stdout = d.shell(['settings', 'put', '--lineage', 'global', 'trust_restrict_usb', '1']).output.strip()
    if "Permission denial" in stdout:
        d.shell('am start -n org.lineageos.lineageparts/.trust.TrustPreferences')
        sleep(1)
        if d(resourceId="android:id/title", text="Restrict USB").exists:
            d(resourceId="android:id/title", text="Restrict USB").click()



def set_locale(d, locale):
    print_step("set locale to "+locale)
    d.shell(['settings', 'put', 'system', 'system_locales', locale])
    print("locale set. will be active after next reboot")

def set_timezone(d, timezone):
    print_step("set timeone to "+timezone)
    d.shell(['setprop', 'persist.sys.timezone', timezone])

def set_time(d):
    print_step("sync time with host")
    time = datetime.datetime.now().isoformat()
    d.shell(['date', '-Is', time])
    d.shell('am broadcast -a android.intent.action.TIME_SET')