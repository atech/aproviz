from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
import socket
from aproviz.utils.console import print_debug, print_error

def wait_for_tor():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(('127.0.0.1',9050))

    if result != 0:
        print("Tor is not running yet but required for login over OIDC")
        print("start tor for example via:")
        print("    $ systemctl start tor")
        print(" ")
        while result != 0:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex(('127.0.0.1',9050))
            sleep(1)
        print("yeah, tor seems to be here now!")


def oidc_login(url, username, password):
    print_debug("OIDC login via "+url)

    wait_for_tor()

    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')
    options.add_argument('--proxy-server=socks5://127.0.0.1:9050')
    driver = webdriver.Chrome(options=options)
    driver.get(url)

    # a login for resetting the device? we need to click a confirm button first
    loginForReset = '/fallback/' in url
    if loginForReset:
        el = driver.find_element(By.CLASS_NAME, 'primary-button')
        el.click()

    if 'protocol/openid-connect/auth' in driver.current_url:

        print_debug("logging in via Keycloak")

        el = driver.find_element(By.ID, 'username')
        el.send_keys(username)
        el = driver.find_element(By.ID, 'password')
        el.send_keys(password)
        sleep(.2) # make it a bit more realistic
        el.submit()
        # TODO
        # if credentials are wrong this currently just throws following error
        #    selenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {"method":"css selector","selector":".primary-button"}
        # 
        # error messgaes should be detected and shown in the console
        sleep(.2)

        if loginForReset:
            if 'you can close this window' in driver.page_source:
                driver.quit()
                return
            else:
                print_error("Could not find confirmation that the window can be closed")
                time.sleep(100)
        

        else:
            el = driver.find_element(By.CLASS_NAME, 'primary-button')
            authUrl = el.get_attribute('href')
            driver.quit()
            return authUrl
    
    else: 
        driver.quit()
        raise Exception('oidc_login: could not detect type of login form (only Keycoak is supported for now)')
