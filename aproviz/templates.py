import json
from typing import Tuple, Any

import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape, StrictUndefined
import os.path
import hashlib
from base64 import b64encode
from os import walk
from aproviz.utils.string import remove_umlaut, remove_non_alphanumeric
from aproviz.config import CONFIG

env = Environment(
    loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), '../templates')),
    autoescape=select_autoescape(),
    undefined=StrictUndefined
)

def topass_filter(value,seed="",length=16):
    encoded=(value+seed+CONFIG['password_seed']).encode()
    result = hashlib.sha256(encoded)
    base = b64encode(result.digest()).decode('utf-8')[::-1]
    return base[0:length] 

def username(value):
    value = value.lower()
    value = remove_umlaut(value)
    value = remove_non_alphanumeric(value)
    return value

env.filters['topass'] = topass_filter
env.filters['username'] = username

def dict_merge(original, update):
    for key, value in original.items(): 
        if key not in update:
            update[key] = value
        elif isinstance(value, dict):
            dict_merge(value, update[key]) 
    return update

def get_template_file(file, context):
    template = env.get_template(file)
    template_rendered = template.render(context)
    data = yaml.load(template_rendered, Loader=yaml.FullLoader)
    return data


def get_template(file, context):
    data = get_template_file(file, context)
    if 'extends' in data:
        p = data['extends']
        del data['extends']
        parent = get_template(p, context)
        data = dict_merge(parent, data)
        data['_templates'].append(file)
    else:
        data['_templates'] = [file]
    return data


def get_template_list():
    f = []
    for (dirpath, dirnames, filenames) in walk(os.path.join(os.path.dirname(__file__), '../templates')):
        f.extend(filenames)
        break
    return f


def get_configs(mod: str, temp: any) -> Tuple[Any, str]:
    """
    Get config for module

    Parameters
    ----------
    mod: str
        module
    temp: Any
        template

    Returns
    -------
    Tuple[Any, str]
        config object and config JSON
    """
    cfg = temp[mod] if mod in temp else {}
    cfg_json = json.dumps(cfg, separators=(',', ':'))
    return cfg, cfg_json


