import os
import subprocess

from aproviz import state
from aproviz.utils.console import print_debug
from aproviz.utils.make_debuggable import patchApk


def apk_install(d, file, debuggable=False):
    if debuggable:
        print("installing " + file + " (debuggable)")
    else:
        print("installing " + file)

    # without root the only way to access an apps /data files are when the app
    # is marked as debuggable
    if debuggable: 
        file_debuggable = file.replace('.apk', '.debuggable.apk')
        if not os.path.isfile(file_debuggable):
            print_debug(file_debuggable+" does not exist yet, patching apk...")
            patchApk(file, file_debuggable, "assets/resign.keystore", "alias_name", "aproviz")
        file  = file_debuggable

    # d.app_install() sometimes fails
    target = "/data/local/tmp/_tmp.apk"
    d.adb_device.push(file, target)
    res =  d.adb_device.shell2(['pm', 'install', "-r", "-t", target])
    if res.returncode != 0:
        raise Exception(res.output)


def apk_download(apk_file, url):
    if os.path.isfile(apk_file):
        print_debug(f"File {url} already cached")
        return

    if state.offline:
        raise Exception(f"Offline mode failed: URL not cached {url}")

    print(f"downloading {url}...")
    p = subprocess.run(['curl', '-f', '-o', apk_file, url])
    if p.returncode:
        raise Exception('download of ' + url + ' failed')
