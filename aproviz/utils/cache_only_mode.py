from importlib import import_module

from aproviz.console import retry_module
from aproviz.templates import get_configs, get_template
from aproviz.utils.console import print_title

DUMMY_DEVICE = devices = {
            'serial': 'DUMMY',
            'model': 'DUMMY',
            'product': 'DUMMY',
            'animal': 'DUMMY',
            'color': 'DUMMY'
        }


def cache_modules(modules, template_file):
    template = get_template(template_file, DUMMY_DEVICE)
    for module in modules:
        print_title(module)
        config, _ = get_configs(module, template)

        while True:
            try:
                m = import_module('aproviz.modules.' + module)
                if 'cache' not in dir(m):
                    print(f"{module} doesn't need caching")
                    break

                m.cache(config)
                break
            except Exception as e:
                retry_module(module, e)