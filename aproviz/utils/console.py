from colorama import Fore, Style

from aproviz.config import CONFIG


def print_title(str):
    print(Fore.GREEN+Style.BRIGHT+"# "+str+Style.RESET_ALL)

def print_step(str):
    print(Fore.BLUE+" % "+str+Style.RESET_ALL)

def print_substep(str):
    print(Fore.BLUE+"   - "+str+Style.RESET_ALL)

def print_success(str):
    print(Fore.GREEN+" "+str+Style.RESET_ALL)

def print_warning(str):
    print(Fore.YELLOW+Style.BRIGHT+"Warning: "+Style.RESET_ALL+str)

def print_error(str):
    print(Fore.RED+Style.BRIGHT+"Error: "+Style.RESET_ALL+str)

def print_warning(str):
    print(Fore.YELLOW+"Warning: "+Style.RESET_ALL+str)

def print_debug(str):
    if 'debug' in CONFIG and CONFIG['debug']:
        print(Style.DIM+"Debug: "+str+Style.RESET_ALL)

def print_key_value(key, value):
    print(Style.BRIGHT+key.rjust(10)+': '+Style.RESET_ALL + value.strip())
