import json
import logging
import os
import re
from pathlib import Path
from typing import Dict, Optional

import requests as r
from requests import Response

from aproviz.utils.console import print_debug
from aproviz.utils.string import remove_suffix


class EtagCaching:
    """
    Provides caching of downloads based on ETag headers
    """
    etags: Dict[str, str] = {}
    cache_dir = ""

    def __init__(self, cache_dir: str):
        """
        Initialize caching of downloads based on ETag headers

        Parameters
        ----------
        cache_dir: str
            directory to use for cache files
        """
        self.cache_dir = cache_dir
        Path(self.cache_dir).mkdir(exist_ok=True, parents=True)
        file = os.path.join(self.cache_dir, 'etags')
        if os.path.isfile(file):
            print_debug("Reading etags from file")
            with open(file, 'r') as f:
                self.etags = json.loads(f.read())
                print_debug("Found etags: " + str(self.etags))

    def fetch_url(self, url: str, offline: Optional[bool] = False) -> str:
        """
        Download and cache the contents of the given

        Parameters
        ----------
        url: str
            URL to download

        offline: bool, optional
            Read from offline cache without requesting online source.
            Defaults to ``false``

        Returns
        -------
        str
            contents from URL
        """
        if offline:
            return self._fetch_offline(url)

        etag: Optional[str] = None

        # Head request
        print_debug("Request ETag from url: " + url)
        h: Response = r.head(url)

        # Check ETag header
        if 'ETag' in h.headers:
            etag = remove_suffix(h.headers['ETag'].strip('"'), "-gzip")
            print_debug("Received ETag: " + etag + " from url: " + url)
            # Check ETag cache
            if url in self.etags and self.etags[url] == etag:
                print_debug("ETag " + etag + " is cached")
                try:
                    # Read cached file
                    return self._read_from_file(url)
                except:
                    print_debug("File not found, downloading from " + url)

        # Download
        print_debug("Downloading url " + url)
        content = r.get(url)
        if content.status_code != 200:
            raise Exception("Could not download url: " + url)

        # Write cache files
        # Caching is useless without ETag
        if etag:
            print_debug("Write file")
            self.etags[url] = etag
            self._write_to_file(url, content.text)

        return content.text

    @staticmethod
    def to_filename(input_str: str) -> str:
        return re.sub('[^a-zA-Z0-9\-_\.]+', '_', input_str)

    def _read_from_file(self, url: str) -> str:
        """
        Read contents of URL from cache file

        Parameters
        ----------
        url: str
            URL that was cached

        Returns
        -------
        str
            cached contents
        """
        log = logging.getLogger(__name__)
        cache_file = os.path.join(self.cache_dir, self.to_filename(url))
        log.debug("read from file " + cache_file)
        if not os.path.isfile(cache_file):
            raise FileNotFoundError
        with open(cache_file, "r") as f:
            content = f.read()
        return content

    def _write_to_file(self, url: str, content: str):
        """
        Write contents to cache file and update ETag cache file

        Parameters
        ----------
        url: str
            downloaded URL
        content: str
            downloaded contents
        """
        # write cache file
        file = os.path.join(self.cache_dir, self.to_filename(url))
        with open(file, 'w') as file1:
            file1.write(content)

        # update ETag cache file
        file = os.path.join(self.cache_dir, 'etags')
        with open(file, 'w') as file2:
            etags_dump = json.dumps(self.etags, separators=(',', ':'))
            print_debug("Dumping Etags: " + etags_dump)
            file2.write(etags_dump)

    def _fetch_offline(self, url):
        if url in self.etags:
            print("Offline mode: Using cached file for url " + url)
            print_debug("ETag is cached for " + url)
            return self._read_from_file(url)
        else:
            raise Exception("Could not find cached version of " + url)
