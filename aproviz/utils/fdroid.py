import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import Dict, List, Optional

from colorama import Fore, Style

from aproviz import state
from aproviz.devices import get_prop
from aproviz.utils.apk import apk_install
from aproviz.utils.console import print_step, print_error, print_debug
from aproviz.utils.etag_caching import EtagCaching

REPOS_CACHE_DIR = os.path.join(os.path.dirname(__file__), '../../cache/repos')

repos: Dict[str, ET.XML] = {}
etag_caching: EtagCaching = EtagCaching(REPOS_CACHE_DIR)


def ask_retry_latest(package_id, required_version):
    print_error("could not find a matching package for " + package_id + " with version " + required_version)
    choice = input(Fore.MAGENTA + '> should we try the latest version instead? (Y/n): ' + Style.RESET_ALL)
    if choice != "y" and choice != "Y" and choice != "":
        sys.exit(1)


def fdroid_get_filename(repo: str, package_id: str, abilist: List[str], required_version: str=None, offline: Optional[bool] = False) -> str:
    """
    Get file of the F-Droid APK
    Parameters
    ----------
    repo: str
        Base URL of the F-Droid repository
    package_id: str
        ID to the app package
    abilist: List[str]
        list of Application Binary Interfaces (ABI).
    required_version: str, optional
        version to install. Defaults to newest

    Returns
    -------
    str
        name of APK file
    """
    root = get_repo(repo, offline)

    for pkg in loop_matching_applications(root, package_id, required_version):
        if not pkg['nativecode']:
            # there is no <nativecode> tag, so every abi should be supported
            return pkg['apkname']
        for abi in pkg['nativecode']:
            if abi in abilist:
                return pkg['apkname']
    if required_version:
        ask_retry_latest(package_id, required_version)
        return fdroid_get_filename(repo, package_id, abilist, offline)
    else:
        raise Exception("could not find a matching package for " + package_id + " with " + ','.join(abilist))


def fdroid_get_all_filenames(repo: str, package_id: str, required_version: str = None) -> List[str]:
    """
    Get F-Droid APK files

    Parameters
    ----------
    repo: str
        Base URL of the F-Droid repository
    package_id: str
        ID to the app package
    required_version: str, optional
        version to install. Defaults to newest

    Returns
    -------
    List[str]
        name of all APK files
    """
    root = get_repo(repo)
    apknames = []
    for pkg in loop_matching_applications(root, package_id, required_version):
        apknames.append(pkg['apkname'])

    if len(apknames):
        return apknames

    if required_version:
        ask_retry_latest(package_id, required_version)
        return fdroid_get_all_filenames(repo, package_id)
    else:
        raise Exception(f"could not find a matching package for {package_id}")


def get_repo(repo: str, offline: Optional[bool] = False) -> ET.Element:
    """
    Fetch F-Droid repository index with cache

    Parameters
    ----------
    repo: str
        Base URL of the F-Droid repository

    offline: bool, optional
        Read from offline cache without requesting online source.
        Defaults to ``false``

    Returns
    -------
    ET.Element
        index XML

    """
    print_debug("Get repo " + repo)
    url = repo + 'index.xml'

    if url in repos:
        print_debug("Found cached repo: " + url)
        return repos[url]

    content = etag_caching.fetch_url(url, offline)
    repos[url] = ET.fromstring(content)
    return repos[url]


lock_file = os.path.join(os.path.dirname(__file__), '../fdroid.lock')


def get_expected_version(package):
    lines = Path(lock_file).read_text().splitlines()
    for line in lines:
        l = line.split('==')
        if l[0] == package:
            return l[1]
    return None


def get_installed_package_version(d, package):
    res = d.shell('dumpsys package ' + package + ' | grep versionName').output
    return res.split('=')[1].strip()


def fdroid_install(d, package, offline: Optional[bool] = state.offline, cache_only: Optional[bool] = False, debuggable: Optional[bool] = False):
    """
    Install app from F-Droid

    Parameters
    ----------
    d
        Device
    package: str
        app package id
    offline: bool, optional
        used cached apk files.
        Defaults to ``state.offline``
    cache_only: bool, optional
        only download files
        Defaults to ``False``
    """
    # get list of installed apps
    print_step('install ' + package)
    repo = 'https://ftp.fau.de/fdroid/repo/'
    if '@' in package:
        package, repo = package.split('@')

    expected_version = get_expected_version(package)

    if not cache_only:
        output, _ = d.shell(['pm', 'list', 'packages'])
        installed = re.findall(r'package:([^\s]+)', output)

        if package in installed:
            if not expected_version or get_installed_package_version(d, package) == expected_version:
                print(package + ' is already installed. skipping')
                return

        abis = get_prop(d, 'ro.product.cpu.abilist').split(',')
        filenames = [fdroid_get_filename(repo, package, abis, expected_version, offline)]
    else:
        filenames = fdroid_get_all_filenames(repo, package, expected_version)

    print_debug(f"Found {len(filenames)} filenames")
    for filename in filenames:
        apk_file = os.path.join(os.path.dirname(__file__), '../../cache', filename)
        print_debug(f"Checking {apk_file}")
        if not os.path.isfile(apk_file):
            if state.offline:
                raise Exception("Offline mode failed: APK file is not cached for ID: " + package + ", filename: " + filename)
            print(f"downloading {filename} ...")
            subprocess.run(['wget', '-O', apk_file, repo + filename])
        else:
            print_debug(f"File {filename} already cached")

        if not cache_only:
            apk_install(d, apk_file, debuggable)


def loop_matching_applications(root, package_id: str, required_version=None):
    result = []
    for child in root.iter('application'):
        if child.attrib['id'] != package_id:
            continue

        for package in child.iter('package'):
            nativecode = None
            apkname = None
            version = None

            for el in package.iter('version'):
                version = el.text
            for el in package.iter('nativecode'):
                nativecode = el.text.split(',')

            if required_version and version != required_version:
                continue

            for el in package.iter('apkname'):
                apkname = el.text

            if not apkname:
                continue

            result.append({'apkname': apkname, 'nativecode': nativecode})
    return result
