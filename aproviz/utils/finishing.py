

from aproviz.utils.console import print_debug

def remove_uiautomator(d):
    d.adb_device.shell(["/data/local/tmp/atx-agent", "server", "--stop"])
    d.adb_device.shell(["rm", "/data/local/tmp/atx-agent"])
    print_debug("atx-agent stopped and removed")
    d.adb_device.shell(["rm", "/data/local/tmp/minicap"])
    d.adb_device.shell(["rm", "/data/local/tmp/minicap.so"])
    d.adb_device.shell(["rm", "/data/local/tmp/minitouch"])
    print_debug("minicap, minitouch removed")
    d.adb_device.shell(["pm", "uninstall", "com.github.uiautomator"])
    d.adb_device.shell(["pm", "uninstall", "com.github.uiautomator.test"])
    print_debug("com.github.uiautomator uninstalled, all done !!!")

def disable_adb(d):
    d.shell(['settings', 'put', 'global', 'adb_enabled', '0'])
