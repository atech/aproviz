

import hashlib
import os


f = open(os.path.join(os.path.dirname(__file__), 'names/animals-de.txt'), 'r')
animals = [line.strip() for line in f.readlines()]
f.close()

f = open(os.path.join(os.path.dirname(__file__), 'names/colors-de.txt'), 'r')
colors = [line.strip() for line in f.readlines()]
f.close()

def str_to_animal(s):
    hashed = int(hashlib.sha1(s.encode('utf-8')).hexdigest(), 16) % len(animals)
    out = animals[hashed]
    while out == '#':
        out = animals[(hashed+1) % len(animals)]
    return out

def str_to_color(s):
    hashed = int(hashlib.sha1(s.encode('utf-8')).hexdigest(), 16) % len(colors)
    out = colors[hashed]
    while out == '#':
        out = colors[(hashed+1) % len(colors)]
    return out

