import time

def hide_notifications_on_lockscreen(d, app, title):
    d.app_stop('com.android.settings')
    d.shell(['am', 'start', '-a', 'android.settings.APP_NOTIFICATION_SETTINGS', '-e', 'android.provider.extra.APP_PACKAGE', app])
    d.shell(['am', 'start', '-a', 'android.settings.APP_NOTIFICATION_SETTINGS', '-e', 'android.provider.extra.APP_PACKAGE', app])
    d(resourceId="com.android.settings:id/entity_header_content").wait()
    time.sleep(1)

    if d.xpath('//*[@resource-id="com.android.settings:id/frame"]/android.widget.Switch[1]').exists and not d(text="Allow notification dot").exists:
        # notifications might be disabled, let's reenable them
        print("enable notifications")
        d.xpath('//*[@resource-id="com.android.settings:id/frame"]/android.widget.Switch[1]').click()
        time.sleep(1)
    
    if d(resourceId="android:id/title", textMatches=title).exists:
        d(resourceId="android:id/title", textMatches=title).click()
    time.sleep(1)
    d(resourceId="com.android.settings:id/silence_label").click()
    if d(scrollable=True).exists:
        d(scrollable=True).scroll.toEnd(steps=2)

    # switch = d.xpath('//*[@resource-id="com.android.settings:id/recycler_view"]/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.Switch[1]')
    # switch = d(resourceId="com.android.settings:id/recycler_view") \
    #     .child_by_text("Minimize", className="android.widget.TextView") \
    #     .child(className="android.widget.Switch")
    # switch = d(resourceId="android:id/switch_widget")
    # print
    # if switch.get_text() == 'OFF':
    #     print("switch click")
    switch = d(textMatches="(Minimize|Minimise|Minimieren)")
    switch.click()
