def grant_permission(d, app, permission):
    d.shell(['pm', 'grant', app, permission])
def ignore_battery_optimizations(d, app):
    d.shell(['dumpsys', 'deviceidle', 'whitelist', '+'+app])