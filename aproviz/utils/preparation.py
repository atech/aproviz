def screen_timeout(d, time):
    d.shell(['settings', 'put', 'system', 'screen_off_timeout', str(time*1000)])   

def disable_rotation(d):
    d.shell(['settings', 'put', 'system', 'accelerometer_rotation', '0'])   

def unlock(d): 
    d.shell(['input', 'keyevent', '82'])
    d.press('back')
    d.unlock()
    d.unlock()
    d.unlock()
    # d.swipe(100, 400, 100, 0, 0.05)
