from string import Template
from aproviz.utils.console import print_error
import tempfile
import os
import sqlite3


_escape_table = [chr(x) for x in range(128)]
_escape_table[0] = "\\0"
_escape_table[ord("\\")] = "\\\\"
_escape_table[ord("\n")] = "\\n"
_escape_table[ord("\r")] = "\\r"
_escape_table[ord("\032")] = "\\Z"
_escape_table[ord('"')] = '\\"'
_escape_table[ord("'")] = "\\'"


def escape_string(value, mapping=None):
    """escapes *value* without adding quote.
    Value should be unicode
    """
    return value.translate(_escape_table)


def sqlite3_query(d, app, file, query, params={}):
    q = Template(query)
    for key, value in params.items():
        params[key] = escape_string(str(value))

    if not file.startswith("/"):
        file = 'databases/'+file

    # copy database out of app data directory
    d.adb_device.shell('rm -f /data/local/tmp/database.db*')
    stdout = d.adb_device.shell('run-as '+app+' cat '+file+' > /data/local/tmp/database.db')
    if len(stdout.strip()) != 0:
        raise Exception("Error: \n"+stdout.strip())

    d.adb_device.shell('run-as '+app+' cat '+file+'-wal > /data/local/tmp/database.db-wal')

    with tempfile.TemporaryDirectory() as tmp:
        path = os.path.join(tmp, 'database.db')
        # download database
        d.pull('/data/local/tmp/database.db', path)
        d.pull('/data/local/tmp/database.db-wal', path+'-wal')

        # execute query
        con = sqlite3.connect(path)
        cur = con.cursor()
        cur.execute(q.substitute(params))
        con.commit()
        con.close()

        # reupload database
        d.push(path, '/data/local/tmp/database.db')


    # copy database back into app data directory
    stdout = d.adb_device.shell('cat /data/local/tmp/database.db | run-as '+app+' tee '+file+' 1>/dev/null')
    if len(stdout.strip()) != 0:
        raise Exception("Error: \n"+stdout.strip())

    # clear sqlite3 WAL 
    d.adb_device.shell('run-as '+app+' rm '+file+'-wal')
