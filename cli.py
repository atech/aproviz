import importlib
import json
import random
import string
import sys
import traceback

import uiautomator2 as u2
import yaml
from colorama import Fore, Style

from aproviz import state
from aproviz.config import CONFIG
from aproviz.console import device_prompt, template_prompt, parse_args, print_device_info, retry_module
from aproviz.devices import lock_device, get_devices, prepare_device, finish_device, store_module_success, connect_wifi, do_checks
from aproviz.modules import get_module_list, execute_module
from aproviz.templates import get_template, get_configs
from aproviz.utils.cache_only_mode import cache_modules
from aproviz.utils.console import print_title, print_error, print_success, print_debug
from aproviz.utils.finishing import disable_adb

available_modules = get_module_list()
args = parse_args(available_modules=available_modules)

if CONFIG['password_seed'] == 'NOTSECURE':
    print("Error: password_seed in config.yml is still set to 'NOTSECURE'. change it to something secure!")
    print("")
    print("Here is a random string for you:")
    random_str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(32))
    print("  " + random_str)
    sys.exit(1)

modules = args.modules if len(args.modules) else available_modules

# order modules
if 'launcher' in modules:
    modules.remove('launcher')
    modules.append('launcher')
if 'system' in modules:
    modules.remove('system')
    modules.append('system')

if args.template:
    template_file = args.template
else:
    template_file = template_prompt()

# Check cache mode
if args.cache_only:
    if args.offline:
        print_error("Conflicting options: --cache-only and --offline. Can't download files in offline mode.")
        sys.exit(1)

    if args.print:
        print_error("Conflicting options: --print and --cache-only")
        sys.exit(1)

    cache_modules(modules, template_file)
    sys.exit(1)

# Check offline mode
if args.offline:
    print_debug("Using offline mode")
    state.offline = True

# Get device
if args.serial:
    devices = get_devices(find_serial=args.serial)
    result = []
    for d in devices:
        if d['d'].serial == args.serial or d['serial'] == args.serial:
            result.append(d)
    if not len(result):
        print_error('could not find device with serial '+args.serial)
        sys.exit(1)
    device = result[0]
else:
    device = device_prompt(modules)

# deactivated for now to avoid confusion
# probably not need anyway

# if device['is_locked']:
#     print("")
#     print(Style.BRIGHT+"Warning: " + Style.RESET_ALL +"device seems to be in use by another aproviz instance.")
#     choice = input(Fore.MAGENTA+'> continue anyway? (y/N): '+Style.RESET_ALL)
#     if choice != "y":
#         sys.exit()

template = get_template(template_file, device)
if args.print:
    print(yaml.dump(template))
    sys.exit()

print_device_info(device, template['_templates'])

lock_device(device['d'])

d = u2.connect(device['d'].serial)
device['ui_d'] = d


# preparation
old_password =""
if 'system' in device['succeeded_modules']:
    system_config = json.loads(device['succeeded_modules']['system'])
    if 'password' in system_config:
        old_password = system_config['password']

prepare_device(d, old_password=old_password)

if 'wifi_ssid' in CONFIG and CONFIG['wifi_ssid']:
    connect_wifi(d, CONFIG['wifi_ssid'], CONFIG['wifi_password'])

try:
    do_checks(d)
except Exception as e:
    print_error(str(e))
    sys.exit(1)


# modules
for module in modules:
    print_title(module)
    config, config_json = get_configs(module, template)

    if (not args.rerun and module in device['succeeded_modules']) and module != 'system':
        if device['succeeded_modules'][module] == config_json:
            print(" Module got executed already and there was no change. skipping")
            continue
        else:
            print(" Module got executed already but we detected a change")


    while True:
        try:
            execute_module(d, module, config)
            break
        except Exception as e:
            retry_module(module, e)


    store_module_success(device['serial'], module, config_json)


# finishing
finish_device(d)

print_device_info(device, template['_templates'])

if not args.keep_adb:
    disable_adb(device['d'])

print_success("device successfully provisioned!")
